// $(".navbar-fixed-top").headroom();

$(function() { 
    var $head = $(".navbar-fixed-top");
    $(window).scroll(function() { 
        
        if ($(window).scrollTop() > 50) {
            if(!$head.hasClass("headroom--unpinned")) {
                $head.addClass("headroom--unpinned")
            }
        } else {
            if($head.hasClass("headroom--unpinned")) {
                $head.removeClass("headroom--unpinned")
            }
        }
    }); 
}); 
