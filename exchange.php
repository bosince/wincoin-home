<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>exchange</title>

    <link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/animate.css/3.5.2/animate.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/page.css">
</head>
<body>
    <?php include 'header.html' ?>
    
    <section class="banner banner-exchange">
        <div class="container">
            <h1>交易所</h1>
            <p>对接全球数字资产交易所，未来将集成超过100+的交易所，让量化交易更多选择，更加有趣。</p>
        </div>
    </section>
    
    <main class="exchange">
        <div class="container">
            <div class="row">
                
            </div>
            <h3>支持的交易所列表</h3>
            <p>一下展示已经或者即将集成的交易所列表及其功能</p>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr class="text_transform">
                            <th>交易所</th>
                            <th>名称</th>
                            <th>K线</th>
                            <th>资金</th>
                            <th>交易</th>
                        </tr>
                    </thead>
                    <tbody class="tbody">
                        <tr>
                            <td>
                                <a href=" https://www.binance.com/" target="_blank">
                                    <img src="assets/img/page/exchange-logo/binance.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Binance</td>
                            <td></td>
                            <td>√</td>
                            <td>√</td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://www.bitfinex.com/" target="_blank">
                                    <img src="assets/img/page/exchange-logo/bitfinex.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Bitfinex</td>
                            <td></td>
                            <td>√</td>
                            <td>√</td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://bittrex.com" target="_blank">
                                    <img src="assets/img/page/exchange-logo/bittrex.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Bittrex</td>
                            <td></td>
                            <td>√</td>
                            <td>√</td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://www.huobi.pro/" target="_blank">
                                    <img src="assets/img/page/exchange-logo/huobi.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Huobi</td>
                            <td></td>
                            <td>√</td>
                            <td>√</td>
                        </tr>
                        <tr>
                            <td>
                                <a href=" https://liqui.io/" target="_blank">
                                    <img src="assets/img/page/exchange-logo/liqui.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Liqui</td>
                            <td></td>
                            <td>√</td>
                            <td>√</td>
                        </tr>
                        <tr>
                            <td>
                                <a href=" https://www.okex.com" target="_blank">
                                    <img src="assets/img/page/exchange-logo/okex.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Okex</td>
                            <td></td>
                            <td>√</td>
                            <td>√</td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://poloniex.com" target="_blank">
                                    <img src="assets/img/page/exchange-logo/poloniex.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Poloniex</td>
                            <td></td>
                            <td>√</td>
                            <td>√</td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://bitflyer.jp/" target="_blank">
                                    <img src="assets/img/page/exchange-logo/bitflyer.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Bitflyer</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://www.bithumb.com/" target="_blank">
                                    <img src="assets/img/page/exchange-logo/bithumb.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Bithumb</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://www.bitstamp.net" target="_blank">
                                    <img src="assets/img/page/exchange-logo/bitstamp.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Bitstamp</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://www.bit-z.com" target="_blank">
                                    <img src="assets/img/page/exchange-logo/bitz.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Bitz</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://cex.io/" target="_blank">
                                    <img src="assets/img/page/exchange-logo/cex.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>CEX.IO</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://www.coinnest.co.kr/" target="_blank">
                                    <img src="assets/img/page/exchange-logo/coinnest.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Coinnest</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://coinone.co.kr/" target="_blank">
                                    <img src="assets/img/page/exchange-logo/coinone.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Coinone</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <a href=" https://www.ethfinex.com/" target="_blank">
                                    <img src="assets/img/page/exchange-logo/ethfinex.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Ethfinex</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <a href=" https://www.gatehub.net/" target="_blank">
                                    <img src="assets/img/page/exchange-logo/gate.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Gate</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://www.gdax.com/" target="_blank">
                                    <img src="assets/img/page/exchange-logo/gdax.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Gdax</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://hitbtc.com" target="_blank">
                                    <img src="assets/img/page/exchange-logo/hibtc.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>Hibtc</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://www.zb.com/" target="_blank">
                                    <img src="assets/img/page/exchange-logo/zb.jpg" alt="" class="img-responsive">
                                </a>
                            </td>
                            <td>ZB</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </main>
    

    <?php include 'footer.html' ?>

    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <script src="assets/js/common.js"></script>
</body>
</html>