<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>token</title>

    <link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/animate.css/3.5.2/animate.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/page.css">

</head>
<body>
    <?php include 'header.html' ?>
    
    <section class="banner banner-token">
        <div class="container">
            <h1>WinCoin 令牌</h1>
            <p>WinCoin 量化交易平台内部，基于以太坊发行的去中心化区块链数字资产，是基于ERC-20 标准协议的Token。</p>
        </div>
    </section>
    
    <main class="token">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="item">
                        <img src="assets/img/page/token01.png" alt="">
                        <h4>总量</h4>
                        <p>根据整个量化交易市场的体量评估，WinCoin 量化交易平台基于ERC-20协议产生10亿WinCoin Token。</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="item">
                        <img src="assets/img/page/token02.png" alt="">
                        <h4>用途</h4>
                        <p>WinCoin 一系列量化交易工具中的内部流通令牌，并将定期销毁部分 WinCoin，直至流通总量保持在5亿枚。</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="item">
                        <img src="assets/img/page/token03.png" alt="">
                        <h4>标准</h4>
                        <p>WinCoin 是基于国际最高标准ERC-20标准协议的智能合约令牌，通过以太区块链网络分发，拥有高度流通性。</p>
                    </div>
                </div>
            </div>

            <div class="row ">
                <h3>WinCoin 持有计划</h3>
                <div class="line2-wrap">
                    <div class="left hidden-xs hidden-sm">
                        <img src="assets/img/page/token11.jpg" alt="">
                    </div>
                    <div class="right">
                        <h4>创始团队 ></h4>
                        <p>为了完成WinCoin 量化交易平台的远期愿景，能让创始团队一直和用户、产品一路长跑，我们将逐步释放锁定的 WinCoin，用于激励忠诚员工和团队，分4年线性释放，每年释放25%。</p>

                        <h4>技术研发、市场及运营 ></h4>
                        <p>为了推动项目的技术开发和市场推广，我们将不遗余力的推进产权和专利的转化，以及营销体系的建立和覆盖，以推动整个量化交易项目在技术层面和用户层面都有量变到质变的飞跃。</p>

                        <h4>客户获取和社区运营 ></h4>
                        <p>对于愿意体验WinCoin 量化交易的亲密客户，我们释放部分Token，以便于他们在未来的量化交易体系中使用 WinCoin Token进行各种量化交易模型的体验。</p>

                        <h4>顾问及合作方 ></h4>
                        <p>WinCoin 的发展永远离不开顾问团队和合作方的支持和帮助，我们将和顾问团队和合作方一起携手相伴，推进整个WinCoin 量化交易平台项目的进程</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-full">
            <div class="row">
                <div class="col-sm-6">
                    <h3>如何获取 WinCoin?</h3>
                    <h4>社区奖励</h4>
                    <p>对平台有突出贡献的用户，如提交更好的交易策略，引荐更好的人才，活跃社区气 氛等，社区都将对其给予一定的 WinCoin 奖励。</p>
                    <h4>从交易所购买</h4>
                    <p>在 WinCoin 上线交易所后，可以用 BTC、ETH 等直接购买。</p>
                </div>
                <div class="col-sm-6">
                    <h3>解禁计划</h3>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>释放期限</td>
                                <td>解禁额度</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>初始释放</td>
                                <td>25%</td>
                            </tr>
                            <tr>
                                <td>一年后释放</td>
                                <td>25%</td>
                            </tr>
                            <tr>
                                <td>两年后释放</td>
                                <td>25%</td>
                            </tr>
                            <tr>
                                <td>三年后释放</td>
                                <td>25%</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td  colspan="2">*除创始团队所持有的部分令牌，其余令牌没有锁定期，可以在 WinCoin 上线交易所后立即交易</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </main>
    

    <?php include 'footer.html' ?>

    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <script src="assets/js/common.js"></script>
</body>
</html>