<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Wincoin</title>

    <link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/animate.css/3.5.2/animate.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/index.css">

</head>
<body>
    <?php include 'header.html' ?>
    

    <section class="banner">
        <div class="container">
            <h2>全球数字货币一站式量化交易平台</h2>
            <h3>开启量化交易之旅，让您轻松获利</h3>
            <!-- <a href="#">白皮书</a> -->
        </div>
    </section>
    
    <section class="module module1">
        <div class="container-full">
            <h2>让数字货币交易更轻松</h2>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="item">
                        <img src="assets/img/module1-01.png" alt="">
                        <h3>智能交易</h3>
                        <p>7*24小时自动监测行情变化，一旦满足策略条件，将自动发起交易。您无需盯盘就能轻松斩获收益。</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="item">
                        <img src="assets/img/module1-02.png" alt="">
                        <h3>简单交易</h3>
                        <p>只需一个账户就能管理您在全球其他交易所的各类账户，任何时间、任何地点您都可以轻松交易。</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="item">
                        <img src="assets/img/module1-03.png" alt="">
                        <h3>安全交易</h3>
                        <p>100%加密，资金仅存放在您开户的交易所账户内，完全在您的掌控之中，我们不接触您的资金。</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="module module2">
        <div class="container-full">
            <h2>对接全球数字资产交易所</h2>
            <h4>未来将集成超过100+的交易所，让量化交易更多选择，更加有趣</h4>
            <div class="row">
                <!-- line 1 -->
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/001.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/002.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/003.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/004.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/005.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/006.jpg" alt="">
                    </div>
                </div>

                <!-- line 2 -->
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/007.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/008.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/009.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/010.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/011.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/012.jpg" alt="">
                    </div>
                </div>

                <!-- line 3 -->
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/013.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/014.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/015.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/016.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/017.jpg" alt="">
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <div class="item">
                        <img src="assets/img/item/018.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="module module3">
        <div class="left"></div>
        <div class="right">
            <h2>更全面的数字资产量化交易平台</h2>
            <h5>100+ 交易所</h5>
            <p>Bithumb, Binance, Poloniex, Bittrex, Bitfinex, Okex, Huobi, Coinone, HitBTC, Bitstamp, Liqui, BTCC, ZB.COM, ...</p>
            <h5>1000+ 数字货币</h5>
            <p>Bitcoin, Ripple, Ethereum, Bitcoin Cash, NEM, Litecoin, Stellar, TRON, IOTA, Dash, EOS, Monero, NEO, Qtum, ...</p>
            <h5>8000+ 交易对 </h5>
            <p>ETH/BTC, TRX/ETH, XRP/BTC, BTC/USDT, XLM/BTC, BNB/BTC, BCC/BTC, IOTA/BTC, ETH/USDT, LTC/BTC, XVG/BTC,...</p>
        </div>
    </section>

    <section class="module module4">
        <div class="container">
            <h2>WinCoin 产品和服务</h2>
            <!-- <h4>可以根据您的量化交易偏好，使用不同模式，不同市场，不同组合，不同币种，全面提升获利空间</h4> -->
            <div class="row">
                <!-- line 1 -->
                <div class="col-xs-6 col-sm-4">
                    <div class="item">
                        <img src="assets/img/module4-01.png" alt="">
                        <!-- <a href="#">跨市套利 ></a> -->
                        <h3>量化投资开发者工具</h3>
                        <p>WinCoin 提供策略编写、回测框架与实盘交易工具，帮助量化交易者不断完善自己的算法</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="item">
                        <img src="assets/img/module4-02.png" alt="">
                        <!-- <a href="#">站内套利 ></a> -->
                        <h3>数字货币投资交流社区</h3>
                        <p>基于用户间Follow功能的数字货币投资社区，帮助投资者发现有价值的交易策略，不断吸引数字货币投资者参与</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="item">
                        <img src="assets/img/module4-03.png" alt="">
                        <!-- <a href="#">趋势交易 ></a> -->
                        <h3>数字货币基金发行工具</h3>
                        <p>为资产管理人提供智能合约和工具，协助发行并运营数字货币基金，并提供业绩鉴证</p>
                    </div>
                </div>

                <!-- line 2 -->
                <div class="col-xs-6 col-sm-4">
                    <div class="item">
                        <img src="assets/img/module4-04.png" alt="">
                        <!-- <a href="#">大数据分析 ></a> -->
                        <h3>数字货币基金加速服务</h3>
                        <p>在平台上筛选表现好、能力强的基金，给予资金、技术和推广支持，并提供风控管理</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="item">
                        <img src="assets/img/module4-05.png" alt="">
                        <!-- <a href="#">交易所 ></a> -->
                        <h3>数字货币基金市场</h3>
                        <p>为投资者提供数字货币基金投资入口，为平台上优秀的基金提供产品展示和销售服务</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="item">
                        <img src="assets/img/module4-06.png" alt="">
                        <!-- <a href="#">跨市套利 ></a> -->
                        <h3>数字货币金融投资产品</h3>
                        <p>开发数字货币投资组合如ETF、FOF等，填补数字货币市场基金产品空缺</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="module module5">
        <div class="container">
            <h2>数字货币投资的市场需求</h2>
            <!-- <div class="row">
                <div class="col-xs-4 col-sm-3 col-md-2">
                    <div class="item">
                        <img src="assets/img/item/5-01.jpg" alt="">
                        <h5>Antonio</h5>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-3 col-md-2">
                    <div class="item">
                        <img src="assets/img/item/5-02.jpg" alt="">
                        <h5>Willie</h5>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-3 col-md-2">
                    <div class="item">
                        <img src="assets/img/item/5-03.jpg" alt="">
                        <h5>Bryan</h5>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-3 col-md-2">
                    <div class="item">
                        <img src="assets/img/item/5-04.jpg" alt="">
                        <h5>Donald</h5>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-3 col-md-2">
                    <div class="item">
                        <img src="assets/img/item/5-05.jpg" alt="">
                        <h5>Harold</h5>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-3 col-md-2">
                    <div class="item">
                        <img src="assets/img/item/5-06.jpg" alt="">
                        <h5>Sebastian</h5>
                    </div>
                </div>
            </div> -->

            <div class="row line1">
                <div class="col-xs-6">
                    <img src="assets/img/module5-01.webp" alt="">
                    <h6 class="bule">普通投资者</h6>
                    <p>普通投资者缺乏加密数字货币投资的知识和技能，急需专业资管服务</p>
                </div>
                <div class="col-xs-6">
                    <img src="assets/img/module5-02.webp" alt="">
                    <h6 class="yellow">专业投资者</h6>
                    <p>专业投资者缺乏连接普通投资者的渠道，并且难以取得投资者信任</p>
                </div>
            </div>
            <div class="row line2">
                <div class="col-xs-4 left">
                    <h6 class="bule">赚多少？</h6>
                    <p>数字货币投资业绩展示与鉴证</p>
                    <h6 class="bule">在哪买？</h6>
                    <p>提供数字货币基金投资入口</p>
                    <h6 class="bule">可信吗？</h6>
                    <p>解决资金安全和信任问题</p>
                </div>
                <div class="col-xs-4">
                    <img src="assets/img/module5-03.png" alt="">
                </div>
                <div class="col-xs-4 right">
                    <h6 class="yellow">基金怎么发？</h6>
                    <p>通过基金发行工具与配套服务</p>
                    <h6 class="yellow">客户怎么找？</h6>
                    <p>通过社区建立与客户的连接</p>
                    <h6 class="yellow">业绩怎么秀？</h6>
                    <p>通过基金市场的业绩排行榜</p>  
                </div>
            </div>
        </div>
    </section>

    <section class="module module6">
        <h2>一站式量化交易，更高效，更便捷</h2>
    </section>

    

    <?php include 'footer.html' ?>

    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <script src="assets/js/common.js"></script>
</body>
</html>