<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>roadmap</title>

    <link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/animate.css/3.5.2/animate.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/page.css">
    <link rel="stylesheet" href="assets/css/timeline.css">

</head>
<body>
    <?php include 'header.html' ?>
    
    <section class="banner banner-roadmap">
        <div class="container">
            <h1>项目发展规划(Roadmap)</h1>
            <p>我们已经在2017年完成了大量程序的代码的开发，并且经过实战套利测算，7-8月份的无风险套利年回报率超过300%。为此，我们更加坚定信心，扩充量化理论导师团队，技术开发团队，力争把Rcash做成全球顶级的量化交易产品。</p>
        </div>
    </section>
    
    <main class="roadmap">
        <section id="cd-timeline" class="cd-container">
            <div class="cd-timeline-block">
                <div class="cd-timeline-img cd-picture">
                    <img src="assets/img/cd-icon-picture.svg" alt="Picture">
                </div>

                <div class="cd-timeline-content">
                    <h2>跨市套利双市场Alpha版（已完成）</h2>
                    <!-- <p>jQuery 团队在官博中再次提醒用户，jQuery 2.0 不再支持IE 6/7/8 了，但是 jQuery 1.9 会继续支持。因为旧版 IE 浏览器在整个互联网中还有很大部分市场，所以他们非常期望大部分网站能继续使用 jQuery 1.x 一段时间。jQuery 团队也将同时支持 jQuery 1.x 和 2.x 。1.9 和 2.0 版的 API 是相同的，所以不必因为你们网站还在用 jQuery 1.9，就感觉好像错过了什么，或者是落后了。</p>
                    <a href="#" class="cd-read-more">阅读更多</a> -->
                    <span class="cd-date">2017-Q2</span>
                </div>
            </div>

            <div class="cd-timeline-block">
                <div class="cd-timeline-img cd-movie">
                    <img src="assets/img/cd-icon-movie.svg" alt="Movie">
                </div>

                <div class="cd-timeline-content">
                    <h2>跨市套利多市场Alpha版（已完成）</h2>
                    <span class="cd-date">2017-Q4</span>
                </div>
            </div>

            <div class="cd-timeline-block">
                <div class="cd-timeline-img cd-picture">
                    <img src="assets/img/cd-icon-picture.svg" alt="Picture">
                </div>

                <div class="cd-timeline-content">
                    <h2>升级量化交易系统模型（已完成）</h2>
                    <h2>API标准建立和市场接入（已完成70%）</h2>
                    <h2>跨市套利多市场Beta版</h2>
                    <span class="cd-date">2018-Q1</span>
                </div>
            </div>

            <div class="cd-timeline-block">
                <div class="cd-timeline-img cd-location">
                    <img src="assets/img/cd-icon-location.svg" alt="Location">
                </div>

                <div class="cd-timeline-content">
                    <h2>站内套利多市场Beta版</h2>
                    <h2>趋势交易（自定义策略外）Alpha版</h2>
                    <span class="cd-date">2018-Q2</span>
                </div>
            </div>

            <div class="cd-timeline-block">
                <div class="cd-timeline-img cd-movie">
                    <img src="assets/img/cd-icon-movie.svg" alt="Movie">
                </div>

                <div class="cd-timeline-content">
                    <h2>趋势交易（自定义策略）Beta版</h2>
                    <h2>其他功能（多市场深度交易，行情预警预测等）</h2>
                    <span class="cd-date">2018-Q3</span>
                </div>
            </div>

            <div class="cd-timeline-block">
                <div class="cd-timeline-img cd-location">
                    <img src="assets/img/cd-icon-location.svg" alt="Location">
                </div>
                

                <div class="cd-timeline-content">
                    <h2>多用户功能 Alpha版</h2>
                    <span class="cd-date">2018-Q4</span>
                </div>
            </div>

            <div class="cd-timeline-block">
                <div class="cd-timeline-img cd-picture">
                    <img src="assets/img/cd-icon-picture.svg" alt="Picture">
                </div>

                <div class="cd-timeline-content">
                    <h2>多用户多功能 Beta版</h2>
                    <span class="cd-date">2019-Q2</span>
                </div>
            </div>

            <div class="cd-timeline-block">
                <div class="cd-timeline-img cd-movie">
                    <img src="assets/img/cd-icon-movie.svg" alt="Movie">
                </div>

                <div class="cd-timeline-content">
                    <h2>多用户多功能 Release版</h2>
                    <span class="cd-date">2019-Q3</span>
                </div>
            </div>
            <div class="cd-timeline-block">
                <div class="cd-timeline-img cd-picture">
                    <img src="assets/img/cd-icon-picture.svg" alt="Picture">
                </div>

                <div class="cd-timeline-content">
                    <h2>量化交易社区、移动Apps</h2>
                    <span class="cd-date">2019-Q4</span>
                </div>
            </div>
        </section>
    </main>
    

    <?php include 'footer.html' ?>

    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <script src="assets/js/common.js"></script>
    <script src="assets/js/modernizr.js"></script>

    <script>
$(function(){
    var $timeline_block = $('.cd-timeline-block');
    //hide timeline blocks which are outside the viewport
    $timeline_block.each(function(){
        if($(this).offset().top > $(window).scrollTop()+$(window).height()*0.75) {
            $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
        }
    });
    //on scolling, show/animate timeline blocks when enter the viewport
    $(window).on('scroll', function(){
        $timeline_block.each(function(){
            if( $(this).offset().top <= $(window).scrollTop()+$(window).height()*0.75 && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) {
                $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
            }
        });
    });
});
</script>
</body>
</html>