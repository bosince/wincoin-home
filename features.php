<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>features</title>

    <link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/animate.css/3.5.2/animate.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/page.css">

</head>
<body>
    <?php include 'header.html' ?>
    
    <section class="banner banner-features">
        <div class="container">
            <h1>平台功能</h1>
            <p>投资者既可以通过购买平台所管理的数字货币量化基金享受到量化交易的收益，也可以付费使用平台所提供的一揽子量化交易工具和交易接口，供投资人进行策略研究、数据建模、交易模型设计、业绩回测、模拟验证、跟踪分析和自动化交易。</p>
        </div>
    </section>
    
    <main class="features">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="item">
                        <img src="assets/img/page/features01.png" alt="">
                        <h4>跨市套利工具</h4>
                        <p>使用平台开发的跨市套利工具，用户可以在多个交易所之间，利用相同交易对之间的差价获利，并让系统根据套利策略买卖下单。</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="item">
                        <img src="assets/img/page/features02.png" alt="">
                        <h4>跨市套利工具</h4>
                        <p>使用平台开发的跨市套利工具，用户可以在多个交易所之间，利用相同交易对之间的差价获利，并让系统根据套利策略买卖下单。</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="item">
                        <img src="assets/img/page/features03.png" alt="">
                        <h4>跨市套利工具</h4>
                        <p>使用平台开发的跨市套利工具，用户可以在多个交易所之间，利用相同交易对之间的差价获利，并让系统根据套利策略买卖下单。</p>
                    </div>
                </div>
                <!-- line 2 -->
                <div class="col-sm-6">
                    <div class="item">
                        <img src="assets/img/page/features04.png" alt="">
                        <h4>跨市套利工具</h4>
                        <p>使用平台开发的跨市套利工具，用户可以在多个交易所之间，利用相同交易对之间的差价获利，并让系统根据套利策略买卖下单。</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="item">
                        <img src="assets/img/page/features05.png" alt="">
                        <h4>跨市套利工具</h4>
                        <p>使用平台开发的跨市套利工具，用户可以在多个交易所之间，利用相同交易对之间的差价获利，并让系统根据套利策略买卖下单。</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="item">
                        <img src="assets/img/page/features06.png" alt="">
                        <h4>跨市套利工具</h4>
                        <p>使用平台开发的跨市套利工具，用户可以在多个交易所之间，利用相同交易对之间的差价获利，并让系统根据套利策略买卖下单。</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    

    <?php include 'footer.html' ?>

    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <script src="assets/js/common.js"></script>
</body>
</html>